//
//  PeaceKit.h
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PeaceKit.
FOUNDATION_EXPORT double PeaceKitVersionNumber;

//! Project version string for PeaceKit.
FOUNDATION_EXPORT const unsigned char PeaceKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PeaceKit/PublicHeader.h>



//
//  BindOperators.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation
import UIKit

import RxSwift
import RxCocoa

// MARK: Operators
// Rx Composition operator which is a shortcut to combineLatest which produces tuple from items
infix operator +

// Rx Drive operator is shortcut for drive and driveNext
infix operator -->

// Rx Chain operator is shortcut for chaining drivers (useful for prapagation of drivers from M to VC in VM)
infix operator ==>

// Rx Dispose operator is a shortcut for addToDisposeBag call
infix operator ~>

// Rx Two way Bind operator between control property and variable
infix operator <->

// MARK: Private Functions
func nonMarkedText(_ textInput: UITextInput) -> String? {
    let start = textInput.beginningOfDocument
    let end = textInput.endOfDocument
    
    guard let rangeAll = textInput.textRange(from: start, to: end),
        let text = textInput.text(in: rangeAll) else {
            return nil
    }
    
    guard let markedTextRange = textInput.markedTextRange else {
        return text
    }
    
    guard let startRange = textInput.textRange(from: start, to: markedTextRange.start),
        let endRange = textInput.textRange(from: markedTextRange.end, to: end) else {
            return text
    }
    
    return (textInput.text(in: startRange) ?? "") + (textInput.text(in: endRange) ?? "")
}

// MARK: + Rx Composition Operator
public func + <A, B>(driverA: Driver<A>, driverB: Driver<B>) -> Driver<(A, B)> {
    return Driver
        .combineLatest(driverA, driverB) { (itemA: A, itemB: B) -> (A, B) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Driver<A?>, driverB: Driver<B?>) -> Driver<(A?, B?)> {
    return Driver
        .combineLatest(driverA, driverB) { (itemA: A?, itemB: B?) -> (A?, B?) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Driver<A>, driverB: Driver<B?>) -> Driver<(A, B?)> {
    return Driver
        .combineLatest(driverA, driverB) { (itemA: A, itemB: B?) -> (A, B?) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Driver<A?>, driverB: Driver<B>) -> Driver<(A?, B)> {
    return Driver
        .combineLatest(driverA, driverB) { (itemA: A?, itemB: B) -> (A?, B) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Variable<A>, driverB: Variable<B>) -> Driver<(A, B)> {
    return Driver
        .combineLatest(driverA.asDriver(), driverB.asDriver()) { (itemA: A, itemB: B) -> (A, B) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Variable<A?>, driverB: Variable<B?>) -> Driver<(A?, B?)> {
    return Driver
        .combineLatest(driverA.asDriver(), driverB.asDriver()) { (itemA: A?, itemB: B?) -> (A?, B?) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Variable<A>, driverB: Variable<B?>) -> Driver<(A, B?)> {
    return Driver
        .combineLatest(driverA.asDriver(), driverB.asDriver()) { (itemA: A, itemB: B?) -> (A, B?) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Variable<A?>, driverB: Variable<B>) -> Driver<(A?, B)> {
    return Driver
        .combineLatest(driverA.asDriver(), driverB.asDriver()) { (itemA: A?, itemB: B) -> (A?, B) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Driver<A>, driverB: Variable<B>) -> Driver<(A, B)> {
    return Driver
        .combineLatest(driverA, driverB.asDriver()) { (itemA: A, itemB: B) -> (A, B) in
            return (itemA, itemB)
    }
}

public func + <A, B>(driverA: Variable<A>, driverB: Driver<B>) -> Driver<(A, B)> {
    return Driver
        .combineLatest(driverA.asDriver(), driverB) { (itemA: A, itemB: B) -> (A, B) in
            return (itemA, itemB)
    }
}

// MARK: --> Rx Drive Operator
public func --> <T>(variable: Variable<T>, observer: @escaping (T) -> Void) -> Disposable {
    return variable.asDriver().drive(onNext: observer)
    //    return variable.asDriver().driveNext(observer)
}

//public func --> <T>(variable: Variable<T>, inout receiver: T) -> Disposable {
//    return variable.asDriver().driveNext { (x: T) -> Void in receiver = x }
//}

public func --> <T,O : ObserverType>(variable: Variable<T>, observer: O) -> Disposable where O.E == T {
    return variable.asDriver().drive(observer)
}

public func --> <T>(variableFrom: Variable<T>, variableTo: Variable<T>) -> Disposable {
    return variableFrom.asDriver().drive(variableTo)
}

public func --> <T>(driver: Driver<T>, observer: @escaping (T) -> Void) -> Disposable {
    return driver.drive(onNext: observer)
    //    return driver.driveNext(observer)
}

//public func --> <T>(driver: Driver<T>, inout receiver: T) -> Disposable {
//    return driver.driveNext { (x: T) -> Void in receiver = x }
//}

public func --> <T,O : ObserverType>(driver: Driver<T>, observer: O) -> Disposable where O.E == T {
    return driver.drive(observer)
}

public func --> <T>(driver: Driver<T>, variable: Variable<T>) -> Disposable {
    return driver.drive(variable)
}

public func --> <T>(driver: Driver<T>, variable: Variable<T?>) -> Disposable {
    return driver
        .map { (item: T) -> T? in
            let itemOpt: T? = item
            return itemOpt
        }
        .drive(variable)
}

// MARK: --> Rx Drive Operator (labels)
public func --> (driver: Driver<String>, label: UILabel) -> Disposable {
    return driver.drive(label.rx.text)
}

public func --> (driver: Driver<String?>, label: UILabel) -> Disposable {
    return driver
        .map { textOpt in
            if let text = textOpt {
                return text
            } else {
                return ""
            }
        }
        .drive(label.rx.text)
}

public func --> (variable: Variable<String>, label: UILabel) -> Disposable {
    return variable.asDriver().drive(label.rx.text)
}

public func --> (variable: Variable<String?>, label: UILabel) -> Disposable {
    return variable
        .asDriver()
        .map { textOpt in
            if let text = textOpt {
                return text
            } else {
                return ""
            }
        }
        .drive(label.rx.text)
}

// MARK: --> Rx Drive Operator (enabled buttons)
public func --> (driver: Driver<Bool>, buttonOpt: UIButton?) -> Disposable? {
    if let button = buttonOpt {
        return driver.drive(button.rx.isEnabled)
    } else {
        return nil
    }
}

public func --> (variable: Variable<Bool>, buttonOpt: UIButton?) -> Disposable? {
    if let button = buttonOpt {
        return variable.asDriver().drive(button.rx.isEnabled)
    } else {
        return nil
    }
}

public func --> (driver: Driver<Bool>, buttonOpt: UIBarButtonItem?) -> Disposable? {
    if let button = buttonOpt {
        return driver.drive(button.rx.isEnabled)
    } else {
        return nil
    }
}

public func --> (variable: Variable<Bool>, buttonOpt: UIBarButtonItem?) -> Disposable? {
    if let button = buttonOpt {
        return variable.asDriver().drive(button.rx.isEnabled)
    } else {
        return nil
    }
}

public func --> (driver: Driver<Bool>, button: UIButton) -> Disposable {
    return driver.drive(button.rx.isEnabled)
}

public func --> (variable: Variable<Bool>, button: UIButton) -> Disposable {
    return variable.asDriver().drive(button.rx.isEnabled)
}

public func --> (driver: Driver<Bool>, button: UIBarButtonItem) -> Disposable {
    return driver.drive(button.rx.isEnabled)
}

public func --> (variable: Variable<Bool>, button: UIBarButtonItem) -> Disposable {
    return variable.asDriver().drive(button.rx.isEnabled)
}

// MARK: ==> Rx Chain Operator
public func ==> <T>(action: Driver<T>, modelDriver: inout Driver<T>) {
    modelDriver = action
}

public func ==> <T: UITextField>(textInput: T, output: inout Driver<String>) {
    output = textInput.rx.text.asDriver().filter { $0 != nil }.map { $0! }
}

public func ==> <T>(action: ControlEvent<T>, modelDriver: inout Driver<T>) {
    modelDriver = action.asDriver()
}

public func ==> <T>(action: Observable<T>, modelDriver: inout Observable<T>) {
    modelDriver = action
}

public func ==> <T>(action: Variable<T>, modelDriver: inout Driver<T>) {
    modelDriver = action.asDriver()
}

public func ==> <T>(action: Variable<T?>, modelDriver: inout Driver<T>) {
    modelDriver = action
        .asDriver()
        .filter { (item: T?) -> Bool in
            return item != nil
        }
        .map { (itemOpt: T?) -> T in
            guard let item = itemOpt else { preconditionFailure("Nil in not allowed in this pipe!") }
            return item
    }
}

public func ==> (btn: UIBarButtonItem, vmAction: inout Driver<Void>) {
    vmAction = btn.rx.tap.asDriver()
}

public func ==> (btn: UIBarButtonItem?, vmAction: inout Driver<Void>) {
    vmAction = btn!.rx.tap.asDriver()
}

public func ==> (btn: UIButton, vmAction: inout Driver<Void>) {
    vmAction = btn.rx.tap.asDriver()
}

public func ==> (btn: UIButton?, vmAction: inout Driver<Void>) {
    vmAction = btn!.rx.tap.asDriver()
}


// MARK: ~> Rx Dispose Operator
public func ~> (disposable: Disposable, bag: DisposeBag) -> Void {
    return disposable.disposed(by: bag)
}

public func ~> (disposableOpt: Disposable?, bag: DisposeBag) -> Void {
    if let disposable = disposableOpt {
        disposable.disposed(by: bag)
    }
}

// MARK: <-> Rx Two-Way Bind Operator
public func <-> (textInput: TextInput<UITextInput>, variable: Variable<String?>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .map { (optValue: String?) -> String in
            if let value = optValue {
                return value
            }
            else {
                return ""
            }
        }
        .bind(to: textInput.text)
    let bindToVariable = textInput.text
        .subscribe(onNext: { [textInput] n in
            //            guard let textInput = textInput else {
            //                return
            //            }
            
            let nonMarkedTextValue = nonMarkedText(textInput.base)
            
            /**
             In some cases `textInput.textRangeFromPosition(start, toPosition: end)` will return nil even though the underlying
             value is not nil. This appears to be an Apple bug. If it's not, and we are doing something wrong, please let us know.
             The can be reproed easily if replace bottom code with
             
             if nonMarkedTextValue != variable.value {
             variable.value = nonMarkedTextValue ?? ""
             }
             
             and you hit "Done" button on keyboard.
             */
            if let nonMarkedTextValue = nonMarkedTextValue, nonMarkedTextValue != variable.value {
                let _: String? = nonMarkedTextValue == "" ? nil : nonMarkedTextValue
                variable.value = nonMarkedTextValue
            }
            }, onCompleted:  {
                bindToUIDisposable.dispose()
        })
    
    return CompositeDisposable.init(bindToUIDisposable, bindToVariable)
}

public enum UserRole {
    case customer
    case staffMember
}

public func <-> (textInput: TextInput<UITextInput>, variable: Variable<String>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .bind(to: textInput.text)
    let bindToVariable = textInput.text
        .subscribe(onNext: { [textInput] n in
            //            guard let textInput = textInput else {
            //                return
            //            }
            
            let nonMarkedTextValue = nonMarkedText(textInput.base)
            
            /**
             In some cases `textInput.textRangeFromPosition(start, toPosition: end)` will return nil even though the underlying
             value is not nil. This appears to be an Apple bug. If it's not, and we are doing something wrong, please let us know.
             The can be reproed easily if replace bottom code with
             
             if nonMarkedTextValue != variable.value {
             variable.value = nonMarkedTextValue ?? ""
             }
             
             and you hit "Done" button on keyboard.
             */
            if let nonMarkedTextValue = nonMarkedTextValue, nonMarkedTextValue != variable.value {
                variable.value = nonMarkedTextValue
            }
            }, onCompleted:  {
                bindToUIDisposable.dispose()
        })
    
    return CompositeDisposable.init(bindToUIDisposable, bindToVariable)
}

public func <-> <T>(property: ControlProperty<T>, variable: Variable<T>) -> Disposable {
    if T.self == String.self {
        #if DEBUG
        fatalError("It is ok to delete this message, but this is here to warn that you are maybe trying to bind to some `rx_text` property directly to variable.\n That will usually work ok, but for some languages that use IME, that simplistic method could cause unexpected issues because it will return intermediate results while text is being inputed.\n REMEDY: Just use `textField <-> variable` instead of `textField.rx_text <-> variable`.\n Find out more here: https://github.com/ReactiveX/RxSwift/issues/649\n"
        )
        #endif
    }
    
    let bindToUIDisposable = variable.asObservable()
        .bind(to: property)
    let bindToVariable = property
        .subscribe(onNext: { n in
            variable.value = n
        }, onCompleted:  {
            bindToUIDisposable.dispose()
        })
    
    //    return CompositeDisposable.create(bindToUIDisposable, bindToVariable)
    return CompositeDisposable.init(bindToUIDisposable, bindToVariable)
}

//
//  Functions.swift
//  PeaceKit
//
//  Created by Martin Muller on 24/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

// Function for formating JSON in console logging in Moya
public func PeaceJSONDebugRDF(_ data: Data) -> Data { // RDF = ResponseDataFormatter
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data, options: [])
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data //fallback to original data if it cant be serialized
    }
}

public func trace(_ identifier: String? = nil, file: String = #file, line: UInt = #line, function: String = #function) {
    if let identifier = identifier {
        print("[PeaceKit] Trace \(identifier)")
    }
    else {
        let trimmedFile: String
        if let lastIndex = file.lastIndexOf("/") {
            trimmedFile = String(file[file.index(after: lastIndex) ..< file.endIndex])
        }
        else {
            trimmedFile = file
        }
        print("[PeaceKit] Trace \(trimmedFile):\(line) \(function)")
    }
}

public func traceClass(_ clazz: String, identifier: String? = nil, file: String = #file, line: UInt = #line, function: String = #function) {
    if let identifier = identifier {
        print("[PeaceKit] Trace \(clazz): \(identifier)")
    }
    else {
        let trimmedFile: String
        if let lastIndex = file.lastIndexOf("/") {
            trimmedFile = String(file[file.index(after: lastIndex) ..< file.endIndex])
        }
        else {
            trimmedFile = file
        }
        print("[PeaceKit] Trace \(trimmedFile):\(line) \(clazz)::\(function)")
    }
}

//
//  Array.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

extension Array {
    public func each(_ task: (_ i: Int, _ item: Element) -> ()) {
        if self.count == 0 { return }
        for i in 0...(self.count-1) {
            task(i, self[i])
        }
    }
    
    public func contains<T>(_ obj: T) -> Bool where T : Equatable {
        return self.filter { $0 as? T == obj }.count > 0
    }
    
    public func first<T: Any>(_ includeElement: (Element) -> Bool) -> T? {
        return self.filter(includeElement).first as? T
    }
    
    public func empty() -> Bool {
        return self.count == 0
    }
}

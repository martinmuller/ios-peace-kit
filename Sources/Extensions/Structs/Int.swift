//
//  Int.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

extension Int {
    public func toString() -> String { return String(self) }
    public func toCGFloat() -> CGFloat { return CGFloat(self) }
    
    public var isEven:     Bool    { return (self % 2 == 0) }
    public var isOdd:      Bool    { return (self % 2 != 0) }
    public var isPositive: Bool    { return (self >= 0) }
    public var isNegative: Bool    { return (self < 0) }
    
    public func times(_ task: (_ i: Int) -> ()) {
        for j in 0...self-1 {
            task(j)
        }
    }
    
    public static func random(_ range: Range<Int>) -> Int {
        var offset = 0
        // allow negative ranges
        if range.lowerBound < 0 { offset = abs(range.lowerBound) }
        
        let mini = UInt32(range.lowerBound + offset)
        let maxi = UInt32(range.upperBound   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
    
    public func repetitions(task: () -> Void) { for _ in 0..<self { task() } }
}

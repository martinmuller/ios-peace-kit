//
//  CLPlacemark.swift
//  PeaceKit
//
//  Created by Martin Muller on 27/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import MapKit

extension CLPlacemark {
    
    func makeAddressString() -> String {
        return [subThoroughfare, thoroughfare, locality, administrativeArea, postalCode, country]
            .compactMap({ $0 })
            .joined(separator: " ")
    }
}

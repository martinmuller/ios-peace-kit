//
//  String.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

extension String {
    public var lowercase: String { return self.lowercased() }
    public var uppercase: String { return self.uppercased() }
    public var capitalize: String { return self.capitalized }
    public var trimmed: String { return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) }
    
    public var length: Int { return self.count }
    
    public var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlHostAllowed)!
    }
    public var UTF8EncodedData: NSData {
        return self.data(using: String.Encoding.utf8)! as NSData
    }
    
    public var stringWithOutDigit: String { return (self.components(separatedBy: NSCharacterSet.decimalDigits) as NSArray).componentsJoined(by: "") }
    
    public func contains(s: String) -> Bool { return self.range(of: s) != nil }
    public func contains(s: String, options: NSString.CompareOptions) -> Bool { return self.range(of: s, options: options) != nil }
    
    public func skipTo(index: String.Index) -> String { return self.substring(from: index) }
    public func skipFrom(index: String.Index) -> String { return self.substring(to: index) }
    
    // MARK: Subscript
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
//    subscript (r: Range<Int>) -> String {
//        let start = index(startIndex, offsetBy: r.lowerBound)
//        let end = index(startIndex, offsetBy: r.upperBound - r.lowerBound)
//        return String(self[Range(start ..< end)])
//    }
    
    public func extractInt() -> Int? {
        let charSet = NSCharacterSet.decimalDigits.inverted
        let string = self.components(separatedBy: charSet).reduce("") { $0 + $1 }
        return string == "" ? nil : Int(string)
    }
    
    public func size(width: CGFloat = CGFloat.greatestFiniteMagnitude, height: CGFloat = CGFloat.greatestFiniteMagnitude, font: UIFont) -> CGSize {
        return (self as NSString).boundingRect(with: CGSize(width: width, height: height),
                                               options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                               attributes: [NSAttributedString.Key.font: font],
                                               context: nil).size
    }
    
    public func isValidEmail() -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func lastIndexOf(_ character: Character) -> Index? {
        var index = endIndex
        while index > startIndex {
            index = self.index(before: index)
            if self[index] == character {
                return index
            }
        }
        
        return nil
    }
    
    // MARK: Date formatter
    
    public var formatForDetail: String { return stringDateFormatter(string: self, type: .Detail) }
    public var formatForReviewCell: String { return stringDateFormatter(string: self, type: .ReviewCell) }
    
    enum DateType {
        case Detail
        case ReviewCell
    }
    
    func stringDateFormatter(string: String, type: DateType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'kk:mm:ss.SSSZ"
        let date = dateFormatter.date(from: string)
        
        return dateToString(date: date as NSDate?, type: type)
    }
    
    func dateToString(date: NSDate?, type: DateType) -> String {
        let dateFormatter = DateFormatter()
        switch type {
        case .Detail:
            dateFormatter.dateFormat = "dd MMM yyyy"
        case .ReviewCell:
            dateFormatter.dateFormat = "dd/MM/yyyy"
        }
        
        if let dateNoOpt = date {
            return dateFormatter.string(from: dateNoOpt as Date)
        } else {
            return ""
        }
    }
    
    public func callNumber() {
        if let url = URL(string: "tel://\(self)".trimmingCharacters(in: .whitespaces)), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) { UIApplication.shared.open(url) }
            else { UIApplication.shared.openURL(url) }
        }
    }
    
    public func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    public func capitalizingFirstLetter() -> String {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    public mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

extension Optional where Wrapped: Equatable {
    public var hasValue: Bool {
        if self == nil { return false }
        return true
    }
}

extension Optional where Wrapped == String {
    public var hasValue: Bool {
        if self == nil || self == "" { return false }
        return true
    }
    
    public func orEmpty() -> String {
        if self != nil { return self! }
        return ""
    }
}

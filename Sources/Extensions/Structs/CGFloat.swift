//
//  CGFloat.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension CGFloat {
    public func changeBy(percentage: CGFloat) -> CGFloat {
        return (self / 100) * percentage
    }
}

//
//  CLLocation.swift
//  PeaceKit
//
//  Created by Martin Muller on 27/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import CoreLocation

extension CLLocation {
    class func distance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to)
    }
}

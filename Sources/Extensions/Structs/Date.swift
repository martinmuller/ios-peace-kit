//
//  Date.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation
import Sugar

extension Date {
    public func isSameDate(as secondDate: Date) -> Bool {
        if self.year == secondDate.year && self.month == secondDate.month && self.day == secondDate.day { return true }
        return false
    }
    
    static public var today: Date { return Date() }
    
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
}

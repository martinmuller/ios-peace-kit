//
//  Double.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

extension Double {
    public func format(_ f: String) -> String { return NSString(format: "%\(f)f" as NSString, self) as String }
    public func round(_ i: Int) -> Double { return NSString(format: "%0.\(i)f" as NSString, self).doubleValue }
    
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

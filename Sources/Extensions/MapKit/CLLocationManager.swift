//
//  CLLocationManager.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation
import MapKit

extension CLLocationManager {
    public var latitude: Double { return location?.coordinate.latitude ?? 0 }
    public var longitude: Double { return location?.coordinate.longitude ?? 0 }
}

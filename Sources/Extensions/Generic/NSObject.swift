//
//  NSObject.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

extension NSObject {
    func with<T: NSObject>(_ modify: (_ s: T) -> ()) -> T {
        modify(self as! T)
        return self as! T
    }
    
    public static func string() -> String { return String(describing: self) }
    public static var name: String { return String(describing: self) }
    public var nameRepresentation: String { return String(describing: type(of: self)) }
}

//
//  Result.swift
//  PeaceKit
//
//  Created by Martin Muller on 24/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

public enum LoadingInfo {
    case inProgress, success, failure, notYet
}

public struct Result<T>: CustomStringConvertible, CustomDebugStringConvertible {
    public let ok: LoadingInfo
    public let message: String?
    public let item: T?
    
    public init(ok: LoadingInfo, message: String?, item: T?) {
        self.ok = ok
        self.message = message
        self.item = item
    }
    
    public var description: String {
        return "\(self.ok): \(String(describing: self.message)) - \(String(describing: self.item))"
    }
    
    public var debugDescription: String {
        return self.description
    }
    
    static public func resultFailed(_ message: String? = nil, item: T? = nil) -> Result { return Result(ok: .failure, message: message, item: item) }
    static public func resultSuccess(_ message: String? = nil, item: T) -> Result { return Result(ok: .success, message: message, item: item) }
    static public func resultNotYet() -> Result { return Result(ok: .notYet, message: nil, item: nil) }
    static public func resultInProgress() -> Result { return Result(ok: .inProgress, message: nil, item: nil) }
    
    static public func duplicate<T, N: Equatable>(result: Result<T>, with data: N) -> Result<N> {
        switch result.ok {
        case .inProgress: return Result<N>.resultInProgress()
        case .success: return Result<N>.resultSuccess(item: data)
        case .failure: return Result<N>.resultFailed(result.message)
        case .notYet: return Result<N>.resultNotYet()
        }
    }
    
    static public func duplicate<T, N: Equatable>(result: Result<T>, with data: N?) -> Result<N?> {
        switch result.ok {
        case .inProgress: return Result<N?>.resultInProgress()
        case .success: return Result<N?>.resultSuccess(item: data)
        case .failure: return Result<N?>.resultFailed(result.message)
        case .notYet: return Result<N?>.resultNotYet()
        }
    }
    
    static public func duplicate<T, N: Equatable>(result: Result<T>, with data: [N]) -> Result<[N]> {
        switch result.ok {
        case .inProgress: return Result<[N]>.resultInProgress()
        case .success: return Result<[N]>.resultSuccess(item: data)
        case .failure: return Result<[N]>.resultFailed(result.message)
        case .notYet: return Result<[N]>.resultNotYet()
        }
    }
}

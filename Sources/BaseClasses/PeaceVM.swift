//
//  PeaceVM.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject

open class PeaceVM: NSObject {
    /// Is this VM active? (should is forward data)
    public var active: Bool
    
    // MARK: Debug Tracing
    open var traceSetup = false
    open var traceRefresh = false
    
    // MARK: Protected Properties
    open var models: [String: PeaceModelP] = [:]
    public let di: Container
    
    public let minimalIntervalRequest       = 1.0
    public let minimalIntervalErrorMessage  = 1.0
    public let minimalIntervalValidation    = 0.2
    
    // MARK: Private Properties
    fileprivate var isValid = true
    
    fileprivate var skipInputProcessingByModelA: Bool = false
    fileprivate var skipInputProcessingByModelB: Bool = false
    
    // MARK: Initialization
    public required init(diProvider: PeaceDIProvider) {
        self.di = diProvider.di
        
        // Not sure if correct way
        self.active = true
        super.init(  )
        
        setupDI(self.di)
        setupModels()
    }
    
    // MARK: Setup
    open func defineModels(_ di: Container) {
        //preconditionFailure("[PeaceKit] Method defineModels() must be overriden by the class \(String(describing: type(of: self)))")
        
        // Example:
        // models["myModel"] = di.resolveMyModel()
    }
    
    // MARK: Public Methods
    
    open func invalidate() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        self.isValid = false
    }
    
    open func refreshIfNeeded() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        if self.isValid == false {
            refresh()
        }
    }
    
    open func refresh() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        performRefresh()
        
        self.isValid = true
    }
    
    /**
     When your VM sets anything in model (a Variable value or Driver of the model)
     you should release those resources in this method.
     
     Otherwise they will be triggered again everytime some other VM will use the model
     - because model instaces are reused in PeaceKit MVVM apps.
     
     - Note: In either case you need to override this method in your VM.
     - Note: VM actions or forwarded drivers do not need to be cleaned
     
     - Warning: Never call this method directly, it is called automatically by PeaceVC in its lifecycle.
     
     - Experiment: Example usage when VM does use Model input
     // Clean up local Variables
     self.inputNewStaffMemberName.value = nil
     
     // Ask model to clear related requests
     modelAddStaffMember().clearRequestAddStaffMember()
     
     - Experiment: Example usage when VM does not pass data to Model
     // noop, there is no need for clean up, this VM does not send data to Model
     */
    open func cleanUp() {
        preconditionFailure("[PeaceKit] Method cleanUp() must be overriden by the class \(String(describing: type(of: self)))")
    }
    
    open func prepareOutputBindings() {
        if self.models.count > 0 {
            bindRefreshToModels()
            
            if traceSetup { traceClass(String(describing: type(of: self))) }
            bindOutputToModels(self.models)
        }
    }
    
    open func processInputBindings() {
        if self.models.count > 0 {
            
            if traceSetup { traceClass(String(describing: type(of: self))) }
            bindInputToModels(self.models)
            
            if self.skipInputProcessingByModelA == false && self.skipInputProcessingByModelB == false {
                if traceSetup { traceClass(String(describing: type(of: self))) }
                
                // Let every model bind the input pipes (set in bindToModels)
                self.models.forEach({ _, model in
                    model.processInputBindings()
                })
            }
        }
    }
    
    // MARK: Protected Methods - Refreshing
    open func performRefresh() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        self.models.forEach({ _, model in
            model.performRefreshDefault()
        })
    }
    
    // MARK: Protected Methods - Setup
    open func setupDI(_ di: Container) {
        // noop - can be used in subclass
    }
    
    open func bindOutputToModels(_ models: [String: PeaceModelP]) {
        // noop - can be used in subclass
        
        // subclass should override this method and it SHOULD NOT call super.bindOutputToModels!
        
        // this code will prevent the model processInputBindings() to be called when not used by this VM
        self.skipInputProcessingByModelA = true
    }
    
    open func bindInputToModels(_ models: [String: PeaceModelP]) {
        // noop - can be used in subclass
        
        // subclass should override this method and it SHOULD NOT call super.bindOutputToModels!
        
        // this code will prevent the model processInputBindings() to be called when not used by this VM
        self.skipInputProcessingByModelB = true
    }
    
    open func configureInput() {
        
        // MARK: After setup is complete `onDidFinishInput` has to be called
    }
    
    // FIXME: Could be improved
    // MARK: Has to be called after input in `configureInput` is configured
    public func onDidFinishInput() {
        models.forEach { _, model in
            model.configureInput()
        }
    }
    
    func setupModels() {
        defineModels(self.di)
    }
    
    func bindRefreshToModels() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        self.models.forEach({ _, model in
            model.prepareOutputBindings() // let model prepare bindings
        })
    }
}

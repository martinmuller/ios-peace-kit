//
//  PeaceAC.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject

open class PeaceAC: NSObject, PeaceACP {
    public let di = Container()
    
    open var rootFlow: PeaceFCP?
    
    weak open var appDelegate: UIApplicationDelegate?
    
    // MARK: Initialization
    public override init() {
        super.init()
        
        printVersion()
        setupDI(self.di)
        defineModels(self.di)
    }
    
    // MARK: Public Methods
    open func createRootVC<T: PeaceVCP>() -> T {
        self.onApplicationStart()
        
        if let flowC = self.rootFlow {
            return flowC.loadInitialVCinBaseFlow() as! T
        } else {
            preconditionFailure("[PeaceKit] setupRootFlow() must be called before createRootVC()")
        }
    }
    
    open func createRootVCinNVC() -> UINavigationController {
        self.onApplicationStart()
        
        if let flowC = self.rootFlow {
            return flowC.loadBaseFlowWithInitialVC()
        } else {
            preconditionFailure("[PeaceKit] setupRootFlow() must be called before createRootVCinNVC()")
        }
    }
    
    open func createRootVCasTabBar() -> UITabBarController {
        self.onApplicationStart()
        
        if let flowC = self.rootFlow {
            return flowC.loadInitialTabBarC()
        } else {
            preconditionFailure("[PeaceKit] setupRootFlow() must be called before createRootVCasTabBar()")
        }
    }
    
    open func createRootVCasPageC() -> UIPageViewController {
        self.onApplicationStart()
        
        if let flowC = self.rootFlow {
            return flowC.loadInitialPageC()
        } else {
            preconditionFailure("[PeaceKit] setupRootFlow() must be called before createRootVCasTabBar()")
        }
    }
    
    open func changeRootVCasTabBar(with rFlow: PeaceFCP) {
        self.rootFlow = rFlow
        
        if let windowOpt = self.appDelegate?.window {
            guard let window = windowOpt else { return }
            
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                window.rootViewController = self?.createRootVCasTabBar()
                }, completion: { completed in })
        }
    }
    
    open func changeRootVCasPage(with rFlow: PeaceFCP) {
        self.rootFlow = rFlow
        
        if let windowOpt = self.appDelegate?.window {
            guard let window = windowOpt else { return }
            
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                window.rootViewController = self?.createRootVCasPageC()
                }, completion: { completed in })
        }
    }
    
    open func changeRootVCinNVC(with rFlow: PeaceFCP) {
        self.rootFlow = rFlow
        
        if let windowOpt = self.appDelegate?.window {
            guard let window = windowOpt else { return }
            
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                window.rootViewController = self?.createRootVCinNVC()
                }, completion: { completed in })
        }
    }
    
    open func setupRootFlow() {
        createRootFlow(self.di)
    }
    
    open func setupServices() {
        // noop - can be used in subclass
    }
    
    open func defineBindings() {
        // noop - can be used in subclass
    }
    
    open func onApplicationStart() {
        self.defineBindings()
    }
    
    public func register<Model: PeaceModel>(model: Model.Type) {
        di.register(model) { [unowned self] (_) -> Model in
            model.init(diProvider: self) }//.inObjectScope(.container)
    }
    
    public func register<FC: PeaceFC>(fc: FC.Type) {
        di.register(fc) { [unowned self] (_) -> FC in
            fc.init(ac: self) }
    }
    
    public func register<Service: PeaceServiceP>(service: Service.Type) {
        di.register(service) { (_) -> Service in
            service.init() }.inObjectScope(.container)
    }
    
    // MARK: Abstract Methods
    open func createRootFlow(_ di: Container) {
        preconditionFailure("[PeaceKit] This method must be overriden by the subclass")
        
        // Example:
        // self.rootFlow = di.resolveBusinessFC()
    }
    
    // MARK: Protected Methods
    open func setupDI(_ di: Container) {
        // noop - can be used in subclass
        
        // Example:
        // di.registerBusinessFC { (resolver) -> (BusinessFC) in
        //     BusinessFC(ac: self)
        // }
    }
    
    open func defineModels(_ di: Container) {
        // noop - can be used in subclass
        
        // Example:
        // self.model = di.resolveMyAppModel()
    }
    
    open func startNetworkActivityIndication() {
        // noop - can be used in subclass
    }
    
    open func stopNetworkActivityIndication() {
        // noop - can be used in subclass
    }
    
    // MARK: Private Methods
    open func printVersion() {
        print("[PeaceKit] Version \(PeaceKitVersionNumber)")
    }
}

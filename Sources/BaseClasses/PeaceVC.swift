//
//  PeaceVC.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject

open class PeaceVC: UIViewController {
    open var traceLifecycle = false
    open var traceSetup = false
    open var traceRefresh = true
    open var traceBinding = false
    var di: Container?
    
    // Protected
    open var viewModel: PeaceVMP?
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        setupInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupInit()
    }
    
    // MARK: Setup
    fileprivate func setupInit() {
        // noop
    }
    
    open func defineViewModel(_ di: Container) -> PeaceVMP {
        preconditionFailure("[PeaceKit] This method must be overriden by the subclass")
        
        // Example:
        // return di.resolveBusinessMapVM()
    }
    
    open func configureInput(_ vm: PeaceVMP) {
        
        // MARK: After setup is complete `onDidFinishInput` has to be called
    }
    
    // FIXME: Could be improved
    // MARK: Has to be called after input in `configureInput` is configured
    public func onDidFinishInput() {
        viewModel?.configureInput()
        viewModel?.onDidFinishInput()
    }
    
    // MARK: View Lifecycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        setupPermissions()
        setupDelegates()
        setupUI()
        
        interConnectLocalUi()
        
        if var vm = self.viewModel {
            vm.active = false
            configureInput(vm)
            onDidFinishInput()
            bindRefreshToViewModel()
            vm.prepareOutputBindings()
            bindToViewModel(vm)
            vm.processInputBindings()
        }
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        updateUI()
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        if var vm = self.viewModel {
            vm.active = true
            
            vm.refreshIfNeeded()
        }
        
        fillUI()
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        if var vm = self.viewModel {
            vm.active = false
            
            vm.cleanUp()
        }
    }
    
    // MARK: Protected Methods
    open func setupPermissions() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func setupDelegates() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func setupUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    /// Warning: A protected method, do not call directly!
    open func updateUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    /// Warning: A protected method, do not call directly!
    open func fillUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func bindRefreshToViewModel() {
        if traceBinding { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
        
        // Example:
        // self.btnRefresh ==> bvm.actionRefreshDefault
    }
    
    /**
     Performs binding of View Controller UI elements and events to provided View Model.
     
     - Warning: A protected method, do not call directly!
     
     - Note: You should use this method to define binding of your View Controller's UI elements
     to View Model's Variables and Drivers
     
     - Parameters:
     - vm: The View Model used for binding
     */
    open func bindToViewModel(_ vm: PeaceVMP) {
        if traceBinding { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func interConnectLocalUi() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    // MARK: Private Methods
    fileprivate func setupViewModel() {
        guard let di = self.di else {
            preconditionFailure("[PeaceKit] DI container must be initialized before View Model can be configured. Call connectToDIContainer() before presenting this VC")
        }
        
        self.viewModel = defineViewModel(di)
    }
}

// MARK: UI & MVVM
extension PeaceVC: PeaceVCP {
    // MARK: Public Methods
    public func connectToDIContainer(_ di: Container) {
        self.di = di
        
        setupViewModel()
    }
}

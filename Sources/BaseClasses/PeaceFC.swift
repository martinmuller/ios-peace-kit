//
//  PeaceFC.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject

public typealias PeaceVCConfig = (_ vc: UIViewController, _ parentFC: PeaceFCP, _ presentingVC: UIViewController?, _ presentingFC: PeaceFCP?) -> Void
public typealias PeaceTabVCIdentification = (fcName: String, vcName: String?)

open class PeaceFC: NSObject, PeaceFCP, PeaceDIRegister {
    
    // MARK: Constants
    public let name: String
    public let ac: PeaceACP
    public static let initialVCConfig = "InitialVC"
    
    // MARK: Variables
    var parent: PeaceFCP?
    var initialVC: PeaceVCP?
    var childFlows: [String: UINavigationController] = [:]
    var childFlowControllers: [String: PeaceFCP] = [:]
    var tabBarVCIds: [PeaceTabVCIdentification] = []
    var pageVCIds: [PeaceTabVCIdentification] = []
    var configurations: [String: PeaceVCConfig] = [:]
    
    open var baseFlow: UINavigationController?
    open var di: Container { return self.ac.di }
    
    public init(name: String, ac: PeaceACP, baseFlow: UINavigationController?) {
        self.name = name
        self.baseFlow = baseFlow
        self.ac = ac
        
        super.init()
        
        setupDI(self.di)
        setupChildFlows(di: self.di)
        setupConfigurations()
        setupTabBar()
        setupPage()
    }
    
    // You should pass FC when you want FC to append his VCs to already existing FC's nvc
    public convenience init(name: String, parent: PeaceFCP) {
        self.init(name: name, ac: parent.ac, baseFlow: parent.baseFlow)
        
        self.parent = parent
    }
    
    public convenience init(name: String, ac: PeaceACP) {
        self.init(name: name, ac: ac, baseFlow: nil)
    }
    
    public required convenience init(ac: PeaceACP) {
        self.init(name: "Unknown", ac: ac)
    }
    
    // MARK: Public Methods
    /// Use this method to instantiate initial FC's VC in current NVC, it will create FC's NVC if needed
    open func loadInitialVCinBaseFlow<T: UIViewController>() -> T {
        return loadInitialVCinBaseFlow(false)
    }
    
    open func loadInitialVCinBaseFlow<T: UIViewController, N: UINavigationController>( _ navigationController: N? = nil) -> T {
        return loadInitialVCinBaseFlow(false, navigationController)
    }
    
    open func loadInitialVCinBaseFlow<T: UIViewController, N: UINavigationController>(_ skipConfigApply: Bool, _ navigationController: N? = nil) -> T {
        let vc = spawnInitialVC(skipConfigApply)
        
        guard let _ = self.baseFlow else {
            if let nvc = navigationController {
                nvc.setViewControllers([vc], animated: false)
                self.baseFlow = nvc
            }
            else {
                self.baseFlow = UINavigationController(rootViewController: vc)
            }
            
            return vc as! T
        }
        
        return vc as! T
    }
    
    open func loadInitialVCinBaseFlow<T: UIViewController>(_ skipConfigApply: Bool) -> T {
        let vc = spawnInitialVC(skipConfigApply)
        
        guard let _ = self.baseFlow else {
            self.baseFlow = UINavigationController(rootViewController: vc)
            
            return vc as! T
        }
        
        return vc as! T
    }
    
    /// Use this method to instantiate initial FC's VC in new NVC
    open func loadInitialVCinChildFlow<T: UIViewController>(_ flowName: String) -> T {
        return loadInitialVCinChildFlow(flowName, skipConfigApply: false)
    }
    
    open func loadInitialVCinChildFlow<T: UIViewController>(_ flowName: String, skipConfigApply: Bool) -> T {
        let vc = spawnInitialVC(skipConfigApply)
        createChildFlowFromVC(flowName, vc: vc)
        return vc as! T
    }
    
    /// Use this method to instantiate initial FC's VC in current NVC
    open func loadBaseFlowWithInitialVC() -> UINavigationController {
        let vc = loadInitialVCinBaseFlow()
        return vc.navigationController!
    }
    
    /// Use this method to instantiate initial FC's VC in new NVC
    open func loadChildFlowWithInitialVC(_ flowName: String) -> UINavigationController {
        let vc = loadInitialVCinChildFlow(flowName)
        return vc.navigationController!
    }
    
    /// Use this method to instantiate initial FC's VC as TabBarC
    open func loadInitialTabBarC() -> UITabBarController {
        let tbc: UITabBarController = spawnInitialVC()
        
        var tabVCs: [UINavigationController] = []
        for tabVCId in self.tabBarVCIds {
            let tabVC = self.flowLoadAsTab(tabVCId.fcName, vcName: tabVCId.vcName, configure: loadConfigurationFor(vcName: tabVCId.vcName))
            
            tabVCs.append(tabVC)
        }
        
        tbc.viewControllers = tabVCs
        
        return tbc
    }
    
    open func loadInitialPageC() -> UIPageViewController {
        let pvc: UIPageViewController = spawnInitialVC()
        
        var pageVCs: [UINavigationController] = []
        for pageVCId in self.pageVCIds {
            let pageVC = self.flowLoadAsPage(pageVCId.fcName, vcName: pageVCId.vcName, configure: loadConfigurationFor(vcName: pageVCId.vcName))
            
            pageVCs.append(pageVC)
        }
        
        let jPvc = pvc as! PeacePVC
        jPvc.pageControllers = pageVCs
        
        return jPvc
    }
    
    open func loadConfigurationFor(vcName: String?) -> PeaceVCConfig? {
        if let name = vcName {
            return self.configurations[name]
        } else {
            return nil
        }
    }
    
    open func loadInitialVC<T: UIViewController>() -> T {
        return loadInitialVC(false)
    }
    
    open func loadInitialVC<T: UIViewController>(_ skipConfigApply: Bool) -> T {
        let vc = spawnInitialVC(skipConfigApply)
        return vc as! T
    }
    
    open func loadVC<T: UIViewController>(_ name: String) -> T {
        return loadVC(name, skipConfigApply: false)
    }
    
    open func loadVC<T: UIViewController>(_ name: String, skipConfigApply: Bool) -> T {
        let vc = spawnVC(name, skipConfigApply: skipConfigApply)
        return vc as! T
    }
    
    open func loadVCinChildFlow<T: UIViewController>(_ name: String, flowName: String? = nil) ->  T {
        return loadVCinChildFlow(name, flowName: flowName, skipConfigApply: false)
    }
    
    open func loadVCinChildFlow<T: UIViewController>(
        _ name: String, flowName: String? = nil, skipConfigApply: Bool
        ) -> T {
        let childFlowName = flowName ?? name
        
        let vc = loadVC(name, skipConfigApply: skipConfigApply)
        createChildFlowFromVC(childFlowName, vc: vc)
        return vc as! T
    }
    
    open func loadChildFlowWithVC(_ name: String, flowName: String? = nil) -> UINavigationController {
        let childFlowName = flowName ?? name
        
        let vc = loadVCinChildFlow(name, flowName: childFlowName)
        return vc.navigationController!
    }
    
    // Use this method to access FC's NVC (should be used only when you passed nvc to init and want to access it from outside)
    open func getBaseFlow() -> UINavigationController {
        guard let baseFlow = self.baseFlow else { preconditionFailure("Base flow for Flow Controller is not initilized yet, pass parent FC or call loadInitialVCinBaseFlow() or loadBaseFlowWithInitialVC() first") }
        
        return baseFlow
    }
    
    // MARK: Protected Methods
    open func setupChildFlows(di: Container) {
        // noop - can be used by subclass
        
        // Example:
        //
        // registerChildFlowController(fc: MacFC, fcName: fcMap)
    }
    
    /// Use this method to setup configurations for VCs loaded from another flows
    /// These configurations will be automatically applied in flowDiveIn and flowOpen
    /// They are applied always BEFORE the optional configure block, so you can always adjust the VC
    /// configuration in the configure block.
    open func setupConfigurations() {
        // noop - can be used by subclass
        
        // Example:
        //
        // registerConfigurationForVC("BusinessShareReviewVC", configuration: PeaceVCConfig)
    }
    
    open func setupTabBar() {
        // noop - can be used by subclass
        
        // Example:
        //
        // registerTabVC(fcMap, vcName: "BusinessShareReviewVC", configuration: PeaceVCConfig)
        // registerTabVC(fcMap, vcName: "BusinessShareReviewVC")
        // registerTabVC(fcMap) // <-- will use FC's initial VC
        // registerTabVC(fcMap, vcName: nil, configuration: PeaceVCConfig) // <-- will use FC's initial VC
    }
    
    open func setupPage() {
        // noop - can be used by subclass
        
        // Example:
        //
        // registerPageVC(fcMap, vcName: "BusinessShareReviewVC", configuration: PeaceVCConfig)
        // registerPageVC(fcMap, vcName: "BusinessShareReviewVC")
        // registerPageVC(fcMap) // <-- will use FC's initial VC
        // registerPageVC(fcMap, vcName: nil, configuration: PeaceVCConfig) // <-- will use FC's initial VC
    }
    
    open func registerConfigurationForVC(_ vcName: String, configuration: @escaping PeaceVCConfig) {
        self.configurations[vcName] = configuration
    }
    
    open func registerTabVC(_ fcName: String, vcName: String, configuration: PeaceVCConfig?) {
        if let config = configuration {
            self.registerConfigurationForVC(vcName, configuration: config)
        }
        
        self.tabBarVCIds.append(PeaceTabVCIdentification(fcName: fcName, vcName: vcName))
    }
    
    open func registerPageVC(_ fcName: String, vcName: String, configuration: PeaceVCConfig?) {
        if let config = configuration {
            self.registerConfigurationForVC(vcName, configuration: config)
        }
        
        self.pageVCIds.append(PeaceTabVCIdentification(fcName: fcName, vcName: vcName))
    }
    
    open func applyConfigurationToVC(_ vcName: String, vc: UIViewController, presentingVC: UIViewController?, presentingFC: PeaceFCP?) {
        if let parentFCConfiguration = self.configurations[vcName] {
            parentFCConfiguration(vc, self, presentingVC, presentingFC)
        }
    }
    
    // MARK: Transition Methods - Base Flow
    open func baseDiveIn(_ vc: UIViewController, animated: Bool = true) {
        guard let baseFlow = self.baseFlow else { print("Base flow is not initialized"); return }
        
        baseFlow.pushViewController(vc, animated: animated) // swiftlint:disable:this force_cast
    }
    
    open func baseBackTo(_ animated: Bool = true) {
        guard let baseFlow = self.baseFlow else { print("Base flow is not initialized"); return }
        
        baseFlow.popViewController(animated: animated) // swiftlint:disable:this force_cast
    }
    
    open func baseOpen(_ vc: UIViewController, animated: Bool = true, transitionStyle: UIModalTransitionStyle = .coverVertical) {
        guard let baseFlow = self.baseFlow else { print("Base flow is not initialized"); return }
        
        if let presentedNVC = vc.navigationController {
            presentedNVC.modalTransitionStyle = transitionStyle
            baseFlow.present(presentedNVC, animated: animated)
        } else {
            vc.modalTransitionStyle = transitionStyle
            baseFlow.present(vc, animated: animated)
        }
    }
    
    open func baseClose(_ animated: Bool = true) {
        guard let baseFlow = self.baseFlow else { print("Base flow is not initialized"); return }
        
        baseFlow.dismiss(animated: animated)
    }
    
    // MARK: Transition Methods - Child Flows
    open func childDiveIn(_ vc: UIViewController, fromFlow: String, animated: Bool = true) {
        guard let childFlow = self.childFlows[fromFlow] else { print("Child flow with name \(fromFlow) does not exist"); return }
        
        childFlow.pushViewController(vc, animated: animated)
    }
    
    open func childBackTo(_ fromFlow: String, animated: Bool = true) {
        guard let childFlow = self.childFlows[fromFlow] else { print("Child flow with name \(fromFlow) does not exist"); return }
        
        childFlow.popViewController(animated: animated) // swiftlint:disable:this force_cast
    }
    
    open func childOpen(_ vc: UIViewController, fromFlow: String, animated: Bool = true, transitionStyle: UIModalTransitionStyle = .coverVertical) {
        guard let childFlow = self.childFlows[fromFlow] else { print("Child flow with name \(fromFlow) does not exist"); return }
        if vc.navigationController === childFlow { preconditionFailure("You are trying to present flow \(fromFlow) on top of itself") }
        
        if let presentedNVC = vc.navigationController {
            presentedNVC.modalTransitionStyle = transitionStyle
            childFlow.present(presentedNVC, animated: animated)
        } else {
            vc.modalTransitionStyle = transitionStyle
            childFlow.present(vc, animated: animated)
        }
    }
    
    open func childClose(_ fromFlow: String, animated: Bool = true) {
        guard let childFlow = self.childFlows[fromFlow] else { print("Child flow with name \(fromFlow) does not exist"); return }
        
        childFlow.dismiss(animated: animated)
    }
    
    // MARK: Transition Methods - Child Flow Controllers
    open func registerChildFlowController(_ fc: PeaceFCP, fcName: String) {
        self.childFlowControllers[fcName] = fc
    }
    
    open func unregisterChildFlowController(_ fcName: String) {
        self.childFlowControllers.removeValue(forKey: fcName)
    }
    
    /// When vcName is nil, the initial VC from child FC will be loaded
    open func flowLoadAsTab(_ fcName: String,
                            vcName: String? = nil,
                            configure: PeaceVCConfig? = nil
        ) -> UINavigationController {
        guard let fc = self.childFlowControllers[fcName] else { preconditionFailure("Child flow controller with name \(fcName) is not registered") }
        
        var newVC: UIViewController? = nil
        var configName: String? = nil
        if let newVCName = vcName {
            //            newVC = fc.loadVC(newVCName, skipConfigApply: true)
            newVC = fc.loadInitialVCinBaseFlow()
            configName = newVCName
        } else {
            //            newVC = fc.loadInitialVC(true)
            newVC = fc.loadInitialVCinBaseFlow()
            configName = PeaceFC.initialVCConfig
        }
        
        
        
        if let childVC = newVC {
            if let newVCName = configName {
                fc.applyConfigurationToVC(newVCName, vc: childVC, presentingVC: nil, presentingFC: self)
            }
            
            if let configureBlock = configure {
                configureBlock(childVC, fc, nil, self)
            }
            
            guard let nvc = childVC.navigationController else {
                preconditionFailure("VC \(String(describing: vcName)) doesnt have NVC")
            }
            
            return nvc
        } else {
            preconditionFailure("VC \(String(describing: vcName)) cannot be loaded from FC \(fc)")
        }
    }
    
    open func flowLoadAsPage(_ fcName: String,
                             vcName: String? = nil,
                             configure: PeaceVCConfig? = nil
        ) -> UINavigationController {
        guard let fc = self.childFlowControllers[fcName] else { preconditionFailure("Child flow controller with name \(fcName) is not registered") }
        
        var newVC: UIViewController? = nil
        var configName: String? = nil
        if let newVCName = vcName {
            newVC = fc.loadInitialVCinBaseFlow()
            configName = newVCName
        } else {
            newVC = fc.loadInitialVCinBaseFlow()
            configName = PeaceFC.initialVCConfig
        }
        
        
        
        if let childVC = newVC {
            if let newVCName = configName {
                fc.applyConfigurationToVC(newVCName, vc: childVC, presentingVC: nil, presentingFC: self)
            }
            
            if let configureBlock = configure {
                configureBlock(childVC, fc, nil, self)
            }
            
            guard let nvc = childVC.navigationController else {
                preconditionFailure("VC \(String(describing: vcName)) doesnt have NVC")
            }
            
            return nvc
        } else {
            preconditionFailure("VC \(String(describing: vcName)) cannot be loaded from FC \(fc)")
        }
    }
    
    /// When vcName is nil, the initial VC from child FC will be loaded
    open func flowDiveIn(_ fcName: String,
                         vcName: String? = nil,
                         fromFlow: String? = nil,
                         inFlow: String? = nil,
                         configure: PeaceVCConfig? = nil,
                         animated: Bool = true)
    {
        guard let fc = self.childFlowControllers[fcName] else { print("Child flow controller with name \(fcName) is not registered"); return }
        
        var newVC: UIViewController? = nil
        var configName: String? = nil
        if let newVCName = vcName {
            newVC = fc.loadVC(newVCName, skipConfigApply: true)
            configName = newVCName
        } else {
            newVC = fc.loadInitialVC(true)
            configName = PeaceFC.initialVCConfig
        }
        
        if let childVC = newVC {
            var presentingVC: UIViewController
            if let fromFlowName = fromFlow { // newVC is presented from child flow
                guard let childFlow = self.childFlows[fromFlowName] else { print("Child flow with name \(fromFlowName) does not exist in FC \(self)"); return }
                guard let topVC = childFlow.topViewController else { print("Top VC for child flow with name \(fromFlowName) does not exist in FC \(self)"); return }
                presentingVC = topVC
            } else { // newVC is presented from base flow
                guard let baseFlow = self.baseFlow else { print("Base flow for FC \(self) does not exist"); return }
                guard let topVC = baseFlow.topViewController else { print("Top VC for base flow in FC \(self) does not exist"); return }
                presentingVC = topVC
            }
            
            if let newVCName = configName {
                fc.applyConfigurationToVC(newVCName, vc: childVC, presentingVC: presentingVC, presentingFC: self)
            }
            
            if let configureBlock = configure {
                configureBlock(childVC, fc, presentingVC, self)
            }
            
            
            if let fromFlowName = inFlow {
                self.childDiveIn(childVC, fromFlow: fromFlowName, animated: animated)
            } else {
                self.baseDiveIn(childVC, animated: animated)
            }
        } else {
            preconditionFailure("VC \(String(describing: vcName)) cannot be loaded from FC \(fc)")
        }
    }
    
    open func flowBackTo(_ fcName: String,
                         fromFlow: String? = nil,
                         animated: Bool = true)
    {
        guard self.childFlowControllers[fcName] != nil else { print("Child flow controller with name \(fcName) is not registered"); return }
        
        var presentedVC: UIViewController
        if let fromFlowName = fromFlow {
            guard let childFlow = self.childFlows[fromFlowName] else { print("Child flow with name \(fromFlowName) does not exist in FC \(self)"); return }
            guard let topVC = childFlow.topViewController else { print("Top VC for child flow with name \(fromFlowName) does not exist in FC \(self)"); return }
            presentedVC = topVC
        } else {
            guard let baseFlow = self.baseFlow else { print("Base flow for FC \(self) does not exist"); return }
            guard let topVC = baseFlow.topViewController else { print("Top VC for base flow in FC \(self) does not exist"); return }
            presentedVC = topVC
        }
        
        if let popedNVC = presentedVC.navigationController {
            popedNVC.popViewController(animated: animated)
        } else {
            print("Child flow controller's VC \(presentedVC) does not have Navigation Controller")
        }
    }
    
    /// When vcName is nil, the initial VC from child FC will be loaded
    open func flowOpen(_ fcName: String,
                       vcName: String? = nil,
                       inFlow: String? = nil,
                       fromFlow: String? = nil,
                       configure: PeaceVCConfig? = nil,
                       animated: Bool = true,
                       transitionStyle: UIModalTransitionStyle = .coverVertical,
                       completion: (() -> Void)? = nil
        ) {
        guard let fc = self.childFlowControllers[fcName] else { print("Child flow controller with name \(fcName) is not registered"); return }
        
        var newVC: UIViewController? = nil
        var configName: String? = nil
        if let newVCName = vcName {
            guard let flowName = inFlow else {
                print("Child flow name has to be provided when just one VC is opened from remote FC"); return }
            
            newVC = fc.loadVCinChildFlow(newVCName, flowName: flowName, skipConfigApply: true)
            
            configName = newVCName
        } else {
            newVC = fc.loadInitialVCinBaseFlow(true)
            configName = PeaceFC.initialVCConfig
        }
        
        if let childVC = newVC {
            var presentingVC: UIViewController
            if let fromFlowName = fromFlow { // newVC is presented from child flow
                guard let childFlow = self.childFlows[fromFlowName] else { print("Child flow with name \(fromFlowName) does not exist in FC \(self)"); return }
                guard let topVC = childFlow.topViewController else { print("Top VC for child flow with name \(fromFlowName) does not exist in FC \(self)"); return }
                presentingVC = topVC
            } else { // newVC is presented from base flow
                guard let baseFlow = self.baseFlow else { print("Base flow for FC \(self) does not exist"); return }
                guard let topVC = baseFlow.topViewController else { print("Top VC for base flow in FC \(self) does not exist"); return }
                presentingVC = topVC
            }
            
            if let newVCName = configName {
                fc.applyConfigurationToVC(newVCName, vc: childVC, presentingVC: presentingVC, presentingFC: self)
            }
            
            if let configureBlock = configure {
                configureBlock(childVC, fc, presentingVC, self)
            }
            
            if let fromFlowName = fromFlow {
                self.childOpen(childVC, fromFlow: fromFlowName, animated: animated, transitionStyle: transitionStyle)
            } else {
                self.baseOpen(childVC, animated: animated, transitionStyle: transitionStyle)
            }
        } else {
            preconditionFailure("VC \(String(describing: vcName)) cannot be loaded from FC \(fc)")
        }
    }
    
    open func flowOpenInitial(_ fcName: String,
                              configure: PeaceVCConfig? = nil,
                              animated: Bool = true,
                              transitionStyle: UIModalTransitionStyle = .coverVertical,
                              completion: (() -> Void)? = nil) {
        self.flowOpen(fcName, vcName: nil, inFlow: nil, fromFlow: nil, configure: configure, animated: animated, transitionStyle: transitionStyle, completion: completion)
    }
    
    // TODO add flowOpenChild
    
    open func flowClose(_ fcName: String,
                        flowName: String? = nil,
                        animated: Bool = true,
                        completion: (() -> Void)? = nil
        ) {
        guard let fc = self.childFlowControllers[fcName] else { print("Child flow controller with name \(fcName) is not registered"); return }
        
        if let closedFlowName = flowName {
            fc.childClose(closedFlowName, animated: animated)
        } else {
            fc.baseClose(animated)
        }
    }
    
    // MARK: Protected Methods
    open func setupDI(_ di: Container) {
        // noop - can be used in subclass
        
        // Example:
        // di.registerBusinessMapVM { (resolver) -> (BusinessMapVM) in
        //     BusinessMapVM(model: di.resolveApiService())
        // }
    }
    
    open func configureInitialVC(_ vc: PeaceVCP) {
        // noop - can be used in subclass
    }
    
    open func startNetworkActivityIndication() {
        self.ac.startNetworkActivityIndication()
    }
    
    open func stopNetworkActivityIndication() {
        self.ac.stopNetworkActivityIndication()
    }
    
    open func removeChildFlow(_ flowName: String) {
        self.childFlows[flowName] = nil
    }
    
    // MARK: Private Methods
    fileprivate func createChildFlowFromVC(_ flowName: String, vc: UIViewController) {
        let nvc = UINavigationController(rootViewController: vc)
        //nvc.extendedLayoutIncludesOpaqueBars = true
        //nvc.navigationBar.translucent = false
        //        nvc.extendedLayoutIncludesOpaqueBars = true // TODO WTF
        //        nvc.navigationBar.isTranslucent = false
        //        self.baseFlow = nvc
        self.childFlows[flowName] = nvc
    }
    
    fileprivate func spawnVC<T: UIViewController>(_ name: String, skipConfigApply: Bool = false) -> T {
        let vc = createVC(name)
        
        let newVC = vc as! PeaceVCP
        newVC.connectToDIContainer(self.di)
        if !skipConfigApply {
            self.applyConfigurationToVC(name, vc: vc, presentingVC: nil, presentingFC: nil)
        }
        return vc as! T
    }
    
    fileprivate func spawnInitialVC<T: UIViewController>(_ skipConfigApply: Bool = false) -> T {
        if let _ = self.initialVC { preconditionFailure("Initial VC is already loaded! (use only one loadInitialVCXXX() method per FC!)") }
        
        let rootVC = createInitialVC()
        let vc = rootVC as! PeaceVCP
        self.initialVC = vc
        
        vc.connectToDIContainer(self.di)
        self.configureInitialVC(vc)
        if !skipConfigApply {
            self.applyConfigurationToVC(PeaceFC.initialVCConfig, vc: rootVC, presentingVC: nil, presentingFC: nil)
        }
        
        return rootVC as! T
    }
    
    open func createVC<T: UIViewController>(_ vc: String) -> T {
        return UIStoryboard(name: name, bundle: nil).instantiateViewController(withIdentifier: vc) as! T
    }
    
    fileprivate func createInitialVC<T: UIViewController>() -> T {
        return UIStoryboard(name: name, bundle: nil).instantiateInitialViewController() as! T
    }
    
    open func isLoggedIn() -> Bool {
        preconditionFailure("[PeaceKit] Method isLoggedIn() must be overriden by the class \(String(describing: type(of: self)))")
    }
    
    open func ensureLogin(_ onDone: () -> (Void)) {
        if (self.isLoggedIn()) {
            onDone()
        } else {
            openAuthenticationFlow(onDone)
        }
    }
    
    open func openAuthenticationFlow(_ onDone: () -> (Void)) {
        preconditionFailure("[PeaceKit] Method openAuthenticationFlow() must be overriden by the class \(String(describing: type(of: self)))")
    }
}

//
//  PeaceModel.swift
//  PeaceKit
//
//  Created by Martin Muller on 24/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject
import RxSwift
import RxCocoa

public enum PeaceModelMode {
    case hot, cold
}

open class PeaceModel: NSObject, PeaceModelP {
    open var traceSetup = false
    open var traceRefresh = false
    open var traceBinding = false
    
    public let di: Container
    
    fileprivate var isBoundRefreshDefault = false
    open var bindingRefreshDefault: Driver<Bool> = Driver.never()
    
    open var actionRefreshDefault: Driver<Void> = Driver.never()
    open var triggerRefreshDefault: Variable<Bool> = Variable(false)
    
    public required init(diProvider: PeaceDIProvider) {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        self.di = diProvider.di
        
        super.init()
        
        setupDI(self.di)
        setupChildModels(self.di)
        
        defineServices(self.di)
    }
    
    open func prepareOutputBindings() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        defineOutputBindings()
    }
    
    // MARK: Setup Methods
    open func setupDI(_ di: Container) {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
        
        // Example:
        // di.registerGeolocationService { (resolver) -> (GeolocationService) in
        //     GeolocationService()
        // }
    }
    
    open func setupChildModels(_ di: Container) {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
        
        // Example:
        // di.registerBusinessModel { (resolver) -> (BusinessModel) in
        //     BusinessModel(diProvider: self)
        // }
    }
    
    // MARK: Define Methods - Services
    open func defineServices(_ di: Container) {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        self.createApiService()
        
        // noop - can be used in subclass
        
        // Example:
        // self.geolocationService = di.resolveGeolocationService()
    }
    
    // MARK: Define Methods - API
    open func defineApiService() {
        //preconditionFailure("[PeaceKit] Method defineApiService() must be overriden by the class \(String(describing: type(of: self)))")
        
        // Example:
        // self.serviceAPI = RxMoyaProvider<UrateUApiService>(
        //   plugins: networkPlugins(debug: false, debugResponseData: false),
        //   endpointClosure: endpointClosure(Defaults[.accessToken])
        // )
    }
    
    open func isApiDefined() -> Bool {
        preconditionFailure("[PeaceKit] Method isApiDefined() must be overriden by the class \(String(describing: type(of: self)))")
        
        // Example:
        // return self.serviceAPI != nil
    }
    
    // MARK: Define Methods - API
    open func defineDefaultMode() -> PeaceModelMode {
        // noop - can be changed in subclass
        
        return .hot
    }
    
    // MARK: Define Methods - Bindings
    open func defineOutputBindings() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
        
        // Here you can define Rx Model bindings when you need to define them in Model, instead of in ViewModel which is the default
    }
    
    open func configureInput() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
    }
    
    open func defineInputBindings() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
        
        // Here you can define Rx Model bindings for input items (= for drivers which are SET after model is setup)
        // Proper call of this method is ensured by calling processInputBindings() from VM
        // Which should be done after bindToModels() of VC execution is ended
        // The needed call to processInputBindings() is done automatically by PeaceVM class
    }
    
    // MARK: Public Methods (called from VM on refresh)
    open func performRefreshDefault() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
    }
    
    // MARK: Protected Methods
    open func createApiService(_ forceReload: Bool = false) {
        if forceReload == true || !isApiDefined() {
            defineApiService()
        }
    }
    
    open func reloadApiService() {
        createApiService(true)
    }
    
    open func processInputBindings() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        defineInputBindings()
    }
    
    open func apiNotFound() -> String {
        return "[PeaceKit] API is not defined, I cannot bind it"
    }
    
    open func serviceNotFound(_ name: String) -> String {
        return "[PeaceKit] Service \(name) is not defined, I cannot bind it"
    }
}

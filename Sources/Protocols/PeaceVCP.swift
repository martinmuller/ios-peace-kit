//
//  PeaceVCP.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject

public protocol PeaceVCP {
    func connectToDIContainer(_ di: Container)
    
    func bindToViewModel(_ vm: PeaceVMP)
}

//
//  PeaceVMP.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

public protocol PeaceVMP: PeaceDIProvider, PeaceDIRegister {
    /// Is this VM active? (should is forward data)
    var active: Bool { get set }
    
    // MARK: Public Methods (called from VC)
    func invalidate() // will cause refresh on next call to refreshIfNeeded()
    func refreshIfNeeded() // perform refresh when VM is invalidated
    func refresh() // perform refresh immediatelly
    func processInputBindings() // every VM needs to process input bindings from VC
    func prepareOutputBindings() // every VM needs to prepare output bindings for VC
    func configureInput()
    func onDidFinishInput()
    
    // MARK: Protected Methods
    func performRefresh() // every VM needs to be able to refresh itself
    func cleanUp() // every VM needs to know how it should be cleaned up on its VC dismiss
}

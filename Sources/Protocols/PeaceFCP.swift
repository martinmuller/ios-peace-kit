//
//  PeaceFCP.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

public protocol PeaceFCP: PeaceDIProvider {
    var ac: PeaceACP { get }
    var baseFlow: UINavigationController? { get }
    
    func loadInitialVCinBaseFlow<T: UIViewController>() -> T
    func loadInitialVCinBaseFlow<T: UIViewController>(_ skipConfigApply: Bool) -> T
    func loadInitialVCinBaseFlow<T: UIViewController, N: UINavigationController>( _ navigationController: N?) -> T
    func loadInitialVCinBaseFlow<T: UIViewController, N: UINavigationController>(_ skipConfigApply: Bool, _ navigationController: N?) -> T
    func loadInitialVCinChildFlow<T: UIViewController>(_ flowName: String) -> T
    func loadInitialVCinChildFlow<T: UIViewController>(_ flowName: String, skipConfigApply: Bool) -> T
    
    func loadBaseFlowWithInitialVC() -> UINavigationController
    func loadChildFlowWithInitialVC(_ flowName: String) -> UINavigationController
    func loadChildFlowWithVC(_ name: String, flowName: String?) -> UINavigationController
    
    func loadInitialTabBarC() -> UITabBarController
    func loadInitialPageC() -> UIPageViewController
    func loadInitialVC<T: UIViewController>() -> T
    func loadInitialVC<T: UIViewController>(_ skipConfigApply: Bool) -> T
    func loadVC<T: UIViewController>(_ name: String) -> T
    func createVC<T: UIViewController>(_ name: String) -> T
    func loadVC<T: UIViewController>(_ name: String, skipConfigApply: Bool) -> T
    func loadVCinChildFlow<T: UIViewController>(_ name: String, flowName: String?) -> T
    func loadVCinChildFlow<T: UIViewController>(_ name: String, flowName: String?, skipConfigApply: Bool) -> T
    
    func getBaseFlow() -> UINavigationController
    
    func startNetworkActivityIndication()
    func stopNetworkActivityIndication()
    
    func applyConfigurationToVC(_ vcName: String, vc: UIViewController, presentingVC: UIViewController?, presentingFC: PeaceFCP?)
    
    func baseDiveIn(_ vc: UIViewController, animated: Bool)
    func baseBackTo(_ animated: Bool)
    func baseOpen(_ vc: UIViewController, animated: Bool, transitionStyle: UIModalTransitionStyle)
    func baseClose(_ animated: Bool)
    
    func childDiveIn(_ vc: UIViewController, fromFlow: String, animated: Bool)
    func childBackTo(_ fromFlow: String, animated: Bool)
    func childOpen(_ vc: UIViewController, fromFlow: String, animated: Bool, transitionStyle: UIModalTransitionStyle)
    func childClose(_ fromFlow: String, animated: Bool)
}

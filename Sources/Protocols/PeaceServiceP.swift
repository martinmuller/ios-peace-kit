//
//  PeaceServiceP.swift
//  PeaceKit
//
//  Created by Martin Muller on 25/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

public protocol PeaceServiceP {
    init()
}

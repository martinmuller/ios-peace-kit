//
//  PeaceDIRegister.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject

public protocol PeaceDIRegister {
    func setupDI(_ di: Container)
}

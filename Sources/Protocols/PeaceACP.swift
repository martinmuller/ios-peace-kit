//
//  PeaceACP.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

import Swinject

public protocol PeaceACP: PeaceDIProvider {
    var rootFlow: PeaceFCP? { get set }
    var appDelegate: UIApplicationDelegate? { get set }
    
    func createRootVC<T: PeaceVCP>() -> T
    func createRootVCinNVC() -> UINavigationController
    func createRootVCasTabBar() -> UITabBarController
    func changeRootVCasTabBar(with rFlow: PeaceFCP)
    func changeRootVCasPage(with rFlow: PeaceFCP)
    func changeRootVCinNVC(with rFlow: PeaceFCP)
    
    func setupRootFlow()
    func setupServices()
    
    func defineBindings()
    
    func onApplicationStart()
    
    func startNetworkActivityIndication()
    func stopNetworkActivityIndication()
}
